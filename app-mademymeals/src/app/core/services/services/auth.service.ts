import { Iuser, Iusersignin } from '../../models/iuser';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
endpoint: string = 'http://localhost:5000/users'
private headers = new HttpHeaders().set('Content-Type', 'application/json');
currentUser: Object = {}; /* res del signin-> token + _id + expires */
  constructor(private http: HttpClient, public router: Router) { }

  public register(user:Iuser) {

return this.http.post(`${this.endpoint}/registerUser`, user).pipe(
  catchError(this.handleError)
)
}

public signin(usersignin: Iusersignin)
{
  return this.http.post<any>(`${this.endpoint}/signin`, usersignin).subscribe((res)=>{
    localStorage.setItem('access_token', res.token)
    this.currentUser = res;
    this.router.navigate([''])
  })
}


public getToken(){
  return localStorage.getItem('acces_token');
}

public doLogout(){
let removeToken = localStorage.removeItem('access_token')
if (removeToken == null) {
  this.router.navigate([''])
}
}

public isLoggedin ():boolean {
 return localStorage.getItem('access_token') != null ? true:false;

}

public getUserProfile(id: string): Observable<any> {
  let api = `${this.endpoint}/user-profile/${id}`;
  return this.http.get(api, { headers: this.headers }).pipe(
    map((res: any) => {
      return res || {}
    }),
    catchError(this.handleError)
  )
}

handleError(error: HttpErrorResponse) {
  let msg = '';
  if (error.error instanceof ErrorEvent) {
    // client-side error
    msg = error.error.message;
  } else {
    // server-side error
    msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  return throwError(msg);
}


}
