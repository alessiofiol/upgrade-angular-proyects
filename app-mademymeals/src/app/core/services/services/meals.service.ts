import { Meals } from '../../../features/Meals/meal/models/meals';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MealsService {
  endpoint: string = 'http://localhost:5000';
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  
  constructor(private http: HttpClient, public router: Router) { }


  public mealregister(meals:Meals) {

    return this.http.post(`${this.endpoint}/meals/createmeal`, meals).pipe(
      catchError(this.handleError)
    )
    }


    public getAllmeal(): Observable<any> {
      let api = `${this.endpoint}/meals`;
      return this.http.get(api, { headers: this.headers }).pipe(
        map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
    }

 // Error 
 handleError(error: HttpErrorResponse) {
  let msg = '';
  if (error.error instanceof ErrorEvent) {
    // client-side error
    msg = error.error.message;
  } else {
    // server-side error
    msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  return throwError(msg);
}

}
