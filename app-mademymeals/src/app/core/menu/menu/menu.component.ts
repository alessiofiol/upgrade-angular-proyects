import { Meals } from '../../../features/Meals/meal/models/meals';
import { AuthService } from '../../services/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { MealsService } from '../../services/services/meals.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  mealslist: Meals[] = [];


  constructor(public authservice:AuthService, public mealservice: MealsService) { }

  ngOnInit(): void {
    this.recoverMeals(); 
  }
logout(){
this.authservice.doLogout();

}

public recoverMeals(){
  return this.mealservice.getAllmeal().subscribe((data)=> {
    this.mealslist = data
})
 }


 
}
