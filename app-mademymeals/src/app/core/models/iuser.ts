export interface Iuser {
    _id: String;
    name: String;
    email: String;
    password: String;
}
export interface Iusersignin {
    email: String;
    password: String;
}