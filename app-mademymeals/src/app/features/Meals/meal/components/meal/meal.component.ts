import { Router } from '@angular/router';
import { MealsService } from '../../../../../core/services/services/meals.service';
import { Meals } from './../../models/meals';
import { Ingredients } from './../../models/ingredients';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.scss']
})
export class MealComponent implements OnInit {
 public ingredientsList: Ingredients[] = [];
 

  public mealform: FormGroup;
  public submitted: boolean = false;
  
  constructor(public mealservice: MealsService, private formbuilder:FormBuilder, public router: Router) {
this.mealform = this.formbuilder.group({
  bread: String,
  meats: String,
  sauces: String,
  extras: String
})
   }

  ngOnInit(): void {
 
  }


public onSubmit (): void {
this.submitted = true;
if (this.mealform.valid){

const mealregister: Meals =
{
  bread: this.breadselection,
  meats: this.meatsselection,
  sauces: this.saucesselection,
  extras: this.extrasselection,
};
this.mealservice.mealregister(mealregister).subscribe((res:any)=>{
 
   
    this.router.navigate([''])
  
})
this.mealform.reset();
this.submitted = false;
}

}

public breadSelector =  [
  {bread: 'sandwich', select: false},
  {bread: 'brioche', select: false},
  {bread: 'baguette', select: false},
  {bread: 'toast', select: false},

];  
public meatsSelector = [
  {meats: 'cow', select: false},
  {meats: 'chicken', select: false},
  {meats: 'turkey', select: false},
  {meats: 'vegan', select: false},

];

public saucesSelector = [
  {sauces: 'ketchup', select: false},
  {sauces: 'mayonesse', select: false},
  {sauces: 'mustard', select: false},
  {sauces: 'ranchera', select: false},

];

public extrasSelector = [
  {extras: 'letucce', select: false},
  {extras: 'cheese', select: false},
  {extras: 'potatoes', select: false},
  {extras: 'tomatoes', select: false},

];

public breadselection:any = [];
public meatsselection:any = [];
public saucesselection:any = [];
public extrasselection:any = [];

onchangefood ($event:any)
{
  const bread = $event.target.value;
  const ischecked = $event.target.checked;

  this.breadSelector = this.breadSelector.map((breadorder)=>
  { if (breadorder.bread == bread) {
      breadorder.select = ischecked;
      if (breadorder.select = ischecked) {
      this.breadselection.push(`${breadorder.bread}`)
     
    } else {this.breadselection.pop()}
      return breadorder  

    }
    //console.log (mealorder)
    return breadorder;
  });



const meats = $event.target.value;
  const ischeckedM = $event.target.checked;

this.meatsSelector = this.meatsSelector.map((meatsorder)=>
  { if (meatsorder.meats == meats) {
      meatsorder.select = ischeckedM;
      if (meatsorder.select = ischeckedM) {
      this.meatsselection.push(`${meatsorder.meats}`)
    
    } else {this.meatsselection.pop()}
      return meatsorder  

    }
    //console.log (mealorder)
    return meatsorder;
  });


  const sauces = $event.target.value;
  const ischeckedS = $event.target.checked;

this.saucesSelector = this.saucesSelector.map((saucesorder)=>
  { if (saucesorder.sauces == sauces) {
      saucesorder.select = ischeckedS;
      if (saucesorder.select = ischeckedS) {
      this.saucesselection.push(`${saucesorder.sauces}`)
  
    } else {this.saucesselection.pop()}
      return saucesorder  

    }
    //console.log (mealorder)
    return saucesorder;
  });


  const extras = $event.target.value;
  const ischeckedE = $event.target.checked;

this.extrasSelector = this.extrasSelector.map((extrasorder)=>
  { if (extrasorder.extras == extras) {
      extrasorder.select = ischeckedE;
      if (extrasorder.select = ischeckedE) {
      this.extrasselection.push(`${extrasorder.extras}`)
    } else {this.extrasselection.pop()}
      return extrasorder  

    }
    //console.log (mealorder)
    return extrasorder;
  });

}



}
