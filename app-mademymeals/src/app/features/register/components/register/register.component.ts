import { Router } from '@angular/router';
import { AuthService } from '../../../../core/services/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
public registerForm!: FormGroup;
  constructor(public authservice:AuthService, public fb:FormBuilder, public router:Router) {
this.buildForm();
   }

  ngOnInit(): void {}
public buildForm () {
  this.registerForm = this.fb.group({
    name: [''],
    email: [''],
    password: [''],
    })
}
public registerUser () {
  this.authservice.register(this.registerForm?.value).subscribe((res:any)=>{
    if (res.result) {
      this.registerForm?.reset();
      this.router.navigate([''])
    }
  })
}
}
