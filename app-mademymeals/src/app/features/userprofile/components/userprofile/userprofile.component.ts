import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/services/services/auth.service';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {
  currentUser: any = {};
  constructor(public authService: AuthService, private actRoute: ActivatedRoute) { 

    let id = this.actRoute.snapshot.paramMap.get('_id')

    this.authService.getUserProfile(id!).subscribe(res => {
     this.currentUser = res.msg;
     console.log(this.currentUser)
    })

  }

  ngOnInit(): void {
  }

}
