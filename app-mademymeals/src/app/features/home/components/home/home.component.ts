import { Meals } from '../../../Meals/meal/models/meals';
import { AuthService } from '../../../../core/services/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { MealsService } from '../../../../core/services/services/meals.service';

@Component({
  selector: 'app-menu',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  mealslist: Meals[] = [];


  constructor(public authservice:AuthService, public mealservice: MealsService) { }

  ngOnInit(): void {
    this.recoverMeals(); 
  }
logout(){
this.authservice.doLogout();

}

public recoverMeals(){
  return this.mealservice.getAllmeal().subscribe((data)=> {
    this.mealslist = data
})
 }


 
}
