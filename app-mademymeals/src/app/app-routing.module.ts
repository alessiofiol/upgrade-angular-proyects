import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  
  {path: ``, loadChildren: () =>
  import('./features/home/home.module').then(m => m.HomeModule)},
  {path: `registerUser`, loadChildren: () =>
  import('./features/register/register.module').then(m => m.RegisterModule)},
  {path: `signin`, loadChildren: () =>
  import('./features/signin/signin.module').then(m => m.SigninModule)},
  {
    path: `user-profile/:_id`, loadChildren: () =>
      import('./features/userprofile/userprofile.module').then(m => m.UserprofileModule),
      canActivate: [AuthGuard] //Con esto protegemos esta ruta
  },
{path: `meal`, loadChildren:() =>
import('./features/Meals/meal/meal.module').then(m => m.MealModule), canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
